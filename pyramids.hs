{- Typ danych reprezentujący planszę, na której ustawiane są wieże -}
data Board = Board [[Int]] deriving Show

{- Funkcja tworząca pustą planszę -}
emptyBoard :: Int -> Board
emptyBoard n = (Board [[0 | x <- [1..n]] | y <- [1..n]])

{- Funkcja zwracająca wiersze planszy -}
getRows :: Board -> [[Int]]
getRows (Board rows) = rows

{- Funkcja zwracająca kolumny planszy -}
getColumns :: Board -> [[Int]]
getColumns (Board rows) = transpose rows

{- Funkcja pomocnicza, zwraca macierz transponowaną -}
transpose :: [[a]] -> [[a]]
transpose []  = []
transpose ([]:xss) = transpose xss
transpose ((x:xs):xss) = (x : [h | (h:t) <- xss]) : transpose (xs : [t | (h:t) <- xss])

{- Zwraca prawdę jeśli piramidę o podanej wysokości można umieścić na danej planszy, na danej pozycji -}
pyramidCanBePlacedAt :: Board -> Int -> Int -> Int -> Bool
pyramidCanBePlacedAt board pyramid rowNo columnNo = ((getRows board) !! rowNo !! columnNo) == 0 && not (isPresentInRow pyramid board rowNo) && not (isPresentInColumn pyramid board columnNo)

{- Zwraca prawdę jeśli wieża o podanej wysokości znajduje się już w danym wierszu -}
isPresentInRow :: Int -> Board -> Int -> Bool
isPresentInRow value (Board rows) rowNo = (isPresentInRow' value rows 0 rowNo)
    where
        isPresentInRow' :: Int -> [[Int]] -> Int -> Int -> Bool
        isPresentInRow' _ [] _ _ = False
        isPresentInRow' value (row:rows) rowNo targetRowNo | rowNo == targetRowNo = value `elem` row
                                                           | otherwise            = isPresentInRow' value rows (rowNo+1) targetRowNo

{- Zwraca prawdę jeśli wieża o podanej wysokości znajduje się już w danej kolumnie -}
isPresentInColumn :: Int -> Board -> Int -> Bool
isPresentInColumn _ (Board []) _ = False
isPresentInColumn value (Board (row:rows)) columnNo = (isPresentInRowAtPosition value row 0 columnNo) || (isPresentInColumn value (Board rows) columnNo)
    where
        isPresentInRowAtPosition :: Int -> [Int] -> Int -> Int -> Bool
        isPresentInRowAtPosition _ [] _ _ = False
        isPresentInRowAtPosition value (x:xs) position targetPosition | position == targetPosition = value == x
                                                                      | otherwise                  = (isPresentInRowAtPosition value xs (position+1) targetPosition)

{- Ustawia piramidę o podanej wysokości na podanej pozycji, zwraca zaktualizowaną planszę -}
putPyramidAt :: Board -> Int -> (Int, Int) -> Board
putPyramidAt (Board rows) pyramid (targetRowNo, targetColumnNo) = Board (iterateRows rows 0)
    where
        iterateRows :: [[Int]] -> Int -> [[Int]]
        iterateRows (row:rows) rowNo | rowNo == targetRowNo = (iterateColumns row 0):rows
                                     | otherwise            = row:(iterateRows rows (rowNo+1))
        iterateColumns :: [Int] -> Int -> [Int]
        iterateColumns (x:xs) columnNo | columnNo == targetColumnNo = pyramid:xs
                                       | otherwise                  = x:(iterateColumns xs (columnNo+1))

---------------------------------------------------------------------------------------------------------------------------------------------------------------------

{- Typ danych reprezentujący ograniczenia łamigłówki (wartości znajdujące się na brzegach planszy, określające ile piramid widać z danego miejsca) -}
data Constraints = Constraints [Maybe Int] [Maybe Int] [Maybe Int] [Maybe Int] deriving Show

{- Tworzy obiekt Constraints na podstawie czterech list zawierających liczbowe ograniczenia. Kolejność ile: góra, dół, lewo, prawp -}
parseConstraints :: [Int] -> [Int] -> [Int] -> [Int] -> Constraints
parseConstraints top bottom left right = (Constraints top' bottom' left' right')
    where
        (top' : bottom' : left' : right' : _) = map intsToMaybe [top, bottom, left, right]
        intsToMaybe :: [Int] -> [Maybe Int]
        intsToMaybe ints = map (\x -> if x == 0 then Nothing else Just x) ints

{- Zwraca prawdę jeśli dana plansza z ustawionymi wieżami spełnia podane ograniczenia -}
checkConstraints :: Board -> Constraints -> Bool
checkConstraints board (Constraints top bottom left right) = (checkElementsFromStart columns top) 
                                                             && (checkElementsFromEnd columns bottom) 
                                                             && (checkElementsFromStart rows left) 
                                                             && (checkElementsFromEnd rows right)
    where
        columns = (getColumns board)
        rows = (getRows board)
        checkElementsFromStart :: [[Int]] -> [Maybe Int] -> Bool
        checkElementsFromStart [] _ = True
        checkElementsFromStart _ [] = True
        checkElementsFromStart (x:xs) (constraint:constraints) = matchesConstraint (visiblePyramidsFromStart x) constraint && checkElementsFromStart xs constraints
        checkElementsFromEnd :: [[Int]] -> [Maybe Int] -> Bool
        checkElementsFromEnd [] _ = True
        checkElementsFromEnd _ [] = True
        checkElementsFromEnd (x:xs) (constraint:constraints) = matchesConstraint (visiblePyramidsFromEnd x) constraint && checkElementsFromEnd xs constraints


{- Zwraca liczbę piramid widzianych "od początku", tzn. od lewej w przypadku wierszy, i od góry w przypadku kolumn -}
visiblePyramidsFromStart :: [Int] -> Int
visiblePyramidsFromStart row = visiblePyramidsFromStart' row []
    where
        visiblePyramidsFromStart' :: [Int] -> [Int] -> Int
        visiblePyramidsFromStart' [] _ = 0
        visiblePyramidsFromStart' (pyramid:pyramids) passedPyramids | all (< pyramid) passedPyramids  = 1 + (visiblePyramidsFromStart' pyramids (pyramid:passedPyramids))
                                                                    | otherwise                        = visiblePyramidsFromStart' pyramids (pyramid:passedPyramids)
                                                                             
{- Zwraca liczbę piramid widzianych "od końca", tzn. od prawej w przypadku wierszy, i od dołu w przypadku kolumn -}
visiblePyramidsFromEnd :: [Int] -> Int
visiblePyramidsFromEnd row = visiblePyramidsFromStart (reverse row)

{- Zwraca prawdę jeśli liczba widzianych piramid spełnia zadanie ograniczenie (jeśli brak ograniczenia, wówczas wartością zawsze jest prawda) -}
matchesConstraint :: Int -> Maybe Int -> Bool
matchesConstraint value (Just constraint) = value == constraint
matchesConstraint value Nothing = True

---------------------------------------------------------------------------------------------------------------------------------------------------------------------

{- Ustawia piramidy na planszy, z zachowaniem podanych ograniczeń. Zwraca ułożoną planszę, bądź obiekt Nothing w przypadku niepowodzenia. -}
solve :: Board -> Constraints -> Maybe Board
solve board constraints = iterateCells board constraints 0 0

{- Iteruje po kolejnych komórkach planszy. Komórki numerowane są zgodnie z kolejnością przeglądania wiersz po wierszu, od lewej do prawej. Dla każdej kolejnej komórki
   wyliczane są jej współrzędne (wiersz oraz kolumna), a następnie następuje próba umieszczenia na tej komórce piramidy w taki sposób, aby wysokości piramid w wierszach
   i kolumnach nie powtarzały się. W przypadku pomyślnego umieszczenia piramidy w komórce, następuje reukrencyjne wywołanie metody iterateCells dla zaktualizowanej planszy
   oraz numeru następnej komórki. Niepowodzenie tej operacji skutkuje natomiast zwróceniem wartości Nothing do poprzedniego poziomu rekurencji. Ów poprzedni poziom, po
   otrzymaniu wartości Nothing z wywołania rekurencyjnego, dokonuje kolejnego podobnego wywołania - tym razem jednak dla niezmodyfikowanej planszy oraz dla zwiększonej
   wartości parametru optionNo. Parametr ten odgrywa kluczową rolę w mechaniźmie nawrotów, stosowanym przez tą metodę, i określa indeks w liście potencjalnych wysokości
   piramid, które mogą być umieszczone w danej komórce. -}
iterateCells :: Board -> Constraints -> Int -> Int -> Maybe Board
iterateCells board constraints cellNo optionNo | cellNo == (n*n) = if (checkConstraints board constraints) then Just board else Nothing
                                               | otherwise       = let (row, column) = cellCoordinates cellNo n
                                                                   in case putSomePyramidAt board n (row, column) optionNo
                                                                      of Just updatedBoard -> case (iterateCells updatedBoard constraints (cellNo+1) 0)
                                                                                              of Just solvedBoard -> Just solvedBoard
                                                                                                 Nothing -> iterateCells board constraints cellNo (optionNo+1)
                                                                         Nothing -> Nothing
    where 
        rows = getRows board
        n = length rows

{- Funkcja pomocnicza dla solve, wyszukuje wszystkie wysokości piramid, które mogą być bezkonfliktowo umieszczone na danej komórce planszy, i wybiera tę, która jest 
   określana przez wartość parametru parametr optionNo. W przypadku braku możliwości umieszczenia jakiejkolwiek piramidy na podanej komórce, bądź też lista potencjalnych
   piramid jest krótsza niż wynikałoby z indeksu optionNo, wówczas zwracana jest wartość Nothing. -}
putSomePyramidAt :: Board -> Int -> (Int, Int) -> Int -> Maybe Board
putSomePyramidAt board n (row, column) optionNo = let possiblePyramids = (filter (\p -> pyramidCanBePlacedAt board p row column) [1..n])
                                                  in if length possiblePyramids > optionNo
                                                     then let pyramid = possiblePyramids !! optionNo in Just (putPyramidAt board pyramid (row, column))
                                                     else Nothing

{- Funkcja pomocnicza dla solve, wylicza współrzędne (wiersz i kolumnę) dla komórki określonej numerem cellNo, znajdującej się na planszy o wymiarach n*n -}
cellCoordinates :: Int -> Int -> (Int, Int)
cellCoordinates cellNo n = (row, column)
    where
        row = cellNo `div` n
        column = cellNo `mod` n


main = do
    putStr "Rozmiar lamiglowki: n = "
    n <- fmap parseInt getLine
    putStr "Wartosci nad gornym wierszem: "
    topConstraints <- fmap parseInts getLine
    putStr "Wartosci pod dolnym wierszem: "
    bottomConstraints <- fmap parseInts getLine
    putStr "Wartosci na lewo od lewej kolumny: "
    leftConstraints <- fmap parseInts getLine
    putStr "Wartosci na prawo od prawej kolumny: "
    rightConstraints <- fmap parseInts getLine

    let constraints = parseConstraints topConstraints bottomConstraints leftConstraints rightConstraints
    let board = emptyBoard n

    print (solve board constraints)

    return ()
    where
        parseInts :: String -> [Int]
        parseInts str = map read (words str)
        parseInt :: String -> Int
        parseInt str = (parseInts str) !! 0