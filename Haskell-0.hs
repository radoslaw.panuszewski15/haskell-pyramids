{- SPOP. Lab 0. Nieoceniane -}

import Data.Char  -- funkcje 'ord' i 'chr' do zadania 5.

{- Zadanie 1. Napisz funkcję, która zwraca środkowy element podanej listy.
Wykorzystaj funkcje standardowe: 'div' i 'length' oraz operator (!!). Przykład:

ghci> middle "Haskell"
'k'
-}

middle :: [a] -> a
middle list = let middleIdx = (length list) `div` 2
              in list !! middleIdx
    
-- main = print (middle "Haskell")

{- Zadanie 2. Napisz funkcję, która usuwa z listy występujące bezpośrednio
po sobie duplikaty danego elementu. Nie korzystaj z funkcji standardowych.
Przykład:

ghci> removeDuplicates [9, 3, 3, 3, 4, 5, 5, 3, 5]
[9,3,4,5,3,5]

Wskazówka: spójrz na definicję funkcji 'maximum' z wykładu. -}

-- removeDuplicates :: Eq a => [a] -> [a]
-- removeDuplicates list = removeDuplicates' list Nothing 
--     where
--         removeDuplicates' [x] _ = [x]
--         removeDuplicates' (x:xs) Nothing = x : removeDuplicates' xs (Just x)
--         removeDuplicates' (x:xs) (Just y) | x == y    = removeDuplicates' xs (Just x)
--                                           | otherwise = x : removeDuplicates' xs (Just x)

removeDuplicates :: Eq a => [a] -> [a]
removeDuplicates [x] = [x]
removeDuplicates (x:y:xs) | x == y    = removeDuplicates (y:xs)
                          | otherwise = x : removeDuplicates(y:xs)

main = (print (removeDuplicates [9, 3, 3, 3, 4, 5, 5, 3, 5]))

{- Zadanie 3. Napisz funkcję, która wstawia do danej listy nowy element
na podanej pozycji. Nie korzystaj z funkcji standardowych. Przykład:

ghci> insertAt "askell" 'H' 0
"Haskell"

Wskazówka: por. z definicją operatora (!!) z wykładu
-}

insertAt :: [a] -> a -> Int -> [a]
insertAt list elem pos = insertAt' list elem pos 0
    where
        insertAt' [] elem _ _ = [elem]
        insertAt' (x:xs) elem pos curPos | pos == curPos  = (elem:x:xs)
                                         | otherwise      = x : insertAt' xs elem pos (curPos + 1) 

-- main = (print (insertAt "askell" 'H' 0))

{- Zadanie 4. Napisz funkcję, która usuwa z listy wszystkie występujące
dalej duplikaty poszczególnych elementów. Przykład:

ghci> removeAllDuplicates [9, 3, 3, 3, 4, 5, 5, 3, 5]
[9,3,4,5]

Wskazówka: spójrz na definicję funkcji 'reverse' z wykładu. W akumulatorze
przechowuj elementy napotykane po raz pierwszy. Użyj funkcji 'elem' do
sprawdzenia, czy element jest już w akumulatorze. -}

removeAllDuplicates :: Eq a => [a] -> [a]
removeAllDuplicates list = removeAllDuplicates' list []
    where
        removeAllDuplicates' [] _ = []
        removeAllDuplicates' (x:xs) [] = x : removeAllDuplicates' xs [x]
        removeAllDuplicates' (x:xs) acc | elem x acc  = removeAllDuplicates' xs acc
                                        | otherwise   = x : removeAllDuplicates' xs (x:acc)

-- main = (print (removeAllDuplicates [9, 3, 3, 3, 4, 5, 5, 3, 5]))

{- Zadanie 5. Zadanie dotyczy szyfrowania tekstów. Prosty kod Cezara polega
na tym, że w miejsce danej litery wstawiamy literę o kodzie większym np.
o 3 (liczbę tę nazywamy kluczem w kodzie Cezara). Końcowe litery alfabetu
zastępujemy literami z początku alfabetu. Np. w miejsce 'A' wstawiamy 'D',
w miejsce 'X' wstawiamy 'A'. Napisz dwie funkcje, które odpowiednio kodują
i dekodują napis szyfrem Cezara o podanym kluczu. Przykład:

ghci> codeCezar "Koty" 3
"Nrwb"
ghci> decodeCezar "Nrwb" 3
"Koty"

Wskazówka: kod ASCII danego znaku zwraca funkcja 'ord :: Char -> Int', natomiast
znak odpowiadający podanemu kodowi ASCII zwraca funkcja 'chr :: Int -> Char'.
Przykład:

ghci> ord 'A'
65
ghci> chr 65
'A' -}

codeCezar :: String -> Int -> String
codeCezar = undefined

decodeCezar :: String -> Int -> String
decodeCezar = undefined
